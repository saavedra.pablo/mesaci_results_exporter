## Quick start

Setting the environment:

```
python3 -m venv mesaci_results_exporter-venv
source mesaci_results_exporter-venv/bin/activate
pip install -r mesaci_results_exporter/requirements.txt

```

export-mesa-gitlab-ci-results:

```
export GITLAB_URL="http://gitlab.freedesktop.org"
export GITLAB_TOKEN="XXXXXXXXX"  ## A Personal token from User Settings -> Access Tokens
export GITLAB_PROJECT="mesa/mesa"
./export-mesa-gitlab-ci-results

Results successfully compressed to: /tmp/tmpdxbqxoy5/results_mesa-129101.psaavedra_ci-intel.129101.tar.xz
Results successfully compressed to: /tmp/tmpgp3jgh8l/results_mesa-129098.psaavedra_ci-intel.129098.tar.xz
Results successfully compressed to: /tmp/tmpxjgv67xx/results_mesa-129089.psaavedra_ci-intel.129089.tar.xz
Results successfully compressed to: /tmp/tmpn88s3m_o/results_mesa-128924.psaavedra_ci-intel.128924.tar.xz
Results successfully compressed to: /tmp/tmp0ytr2h3x/results_mesa-128813.psaavedra_ci-intel.128813.tar.xz
Results successfully compressed to: /tmp/tmpeqz7q3nq/results_mesa-127397.psaavedra_ci-intel.127397.tar.xz
Results successfully compressed to: /tmp/tmphffj6jo3/results_mesa-127385.psaavedra_ci-intel.127385.tar.xz
```

webhook-mesa-gitlab-ci-result:

```
export GITLAB_URL="http://gitlab.freedesktop.org"
export GITLAB_TOKEN="XXXXXXXXX"  ## A Personal token from User Settings -> Access Tokens
export GITLAB_PROJECT="mesa/mesa"
export GITLAB_WEBHOOK_SECRET_TOKEN="AijIkEjVuAflakbarlAidJahiankIkVa"

./webhook-mesa-gitlab-ci-result

Bottle v0.12.18 server starting up (using WSGIRefServer())...
Listening on http://0.0.0.0:8081/
Hit Ctrl-C to quit.

```
