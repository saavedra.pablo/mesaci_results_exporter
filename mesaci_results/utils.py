# -*- coding:utf-8 -*-

# Author: Pablo Saavedra <psaavedra@igalia.com>
# Maintainer: Pablo Saavedra <psaavedra@igalia.com>

import json
import os
import tarfile
import tempfile
import time
import subprocess
import zipfile


def sanatize_name(name):
    return name.replace("/", "_")


def translate_job_status(status):
    if status == "failed":
        return "fail"
    return status


def get_timestamp(datestring, format="%Y-%m-%dT%H:%M:%S.%f"):
    if datestring[-1] == "Z":
        datestring = datestring[:-1]
    return time.mktime(time.strptime(datestring, format))


class GitlabSyncer:

    def __init__(self, project):
        self.project = project

    def get_pipelines_from_given_id(self, id=None, limit=None):
        '''
        Returns the pipelines from a given minor pipeline id nunber passed
        as argument

        #TODO this must to have in account the 'on going` tasks to ask for them
        later.
        '''
        per_page = 50
        page = 1
        counter = 1
        while True:
            pipelines = self.project.pipelines.list(page=page,
                                                    per_page=per_page)
            for pipeline in pipelines:
                if limit and counter > limit:
                    return
                if pipeline.get_id() <= id:
                    return
                yield pipeline
                counter += 1
            page += 1
            if not len(pipelines):
                return

    def get_job_artifacts(self, job, download_dir):
        artifacts = []
        artifacts_log_counter = 0
        artifacts_log_testcases = ""
        file_name = download_dir + '/artifacts.zip'
        results_dir = download_dir + "/results"
        with open(file_name, "wb") as f:
            job_aux = self.project.jobs.get(job.get_id(), lazy=True)
            job_aux.artifacts(streamed=True, action=f.write)
        zip = zipfile.ZipFile(file_name)
        zip.extractall(download_dir)
        os.chdir(download_dir + "")
        for root, adir, files in os.walk(os.path.normpath(results_dir)):
            if len(files) < 1:
                continue
            for file in files:
                base, ext = os.path.splitext(file)
                if ext not in [".log", ".xml"]:
                    continue
                artifact = {}
                artifact["type"] = "junit" if ext == "xml" else "log"
                artifact["file"] = (root + os.sep + file)[len(results_dir)+1:]
                artifacts.append(artifact)

                if artifact["type"] != "junit":

                    artifacts_log_counter += 1
                    artifacts_log_testcases += '''
        <testcase classname="%s"
            name="%s"
            status="%s"
            time="%s">
        </testcase>''' % (job.attributes["name"],
                          base,
                          translate_job_status(job.attributes["status"]),
                          job.attributes["duration"])

        # TODO: Create a syntetic XML.
        if artifacts_log_counter > 0:
            xml_file_content = '''
<testsuites>
    <testsuite name="%s" tests="%s">
    %s
    </testsuite>
</testsuites>''' % (job.attributes["name"],
                    artifacts_log_counter,
                    artifacts_log_testcases)
            xml_file_name = "%s-%s.xml" % (job.attributes["name"],
                                           job.attributes["id"])
            xml_file = results_dir + "/" + xml_file_name
            with open(xml_file, "w") as fh:
                fh.write(xml_file_content)

            artifact = {}
            artifact["type"] = "junit"
            artifact["file"] = xml_file_name
            artifacts.append(artifact)

        return artifacts

    def get_components(self, pipeline, workdir):
        components = {}
        jobs = pipeline.jobs.list(all=True)
        for job in jobs:
            if job.attributes["status"] not in ["success", "failed"]:
                continue
            if 'artifacts_file' not in job.attributes:
                continue
            if job.attributes['artifacts_file']["filename"].find("traces") < 0:
                continue

            component = {
                "build": job.attributes['id'],
                "machine": "docker",  # TODO
                "name": job.attributes['name'],
                "shard": "0",  # TODO
                "arch": "m64",  # TODO
                "hardware": "unset",  # TODO
                "status": translate_job_status(job.attributes['status']),
                "url": job.attributes['web_url'],
                "start_time": get_timestamp(job.attributes['created_at']),
                "end_time": get_timestamp(job.attributes['finished_at']),
                "trigger_name": "none",  # TODO
            }
            component["artifacts"] = self.get_job_artifacts(job, workdir)
            components[component['name']] = component

        return components

    def generate_build_info(self, pipeline, workdir):
        revisions = {
            "project1": {
                "author": "pablos",
                "commit": "project1=sha1",
                "sha": "sha1",
                "description": "blah blah"
            }
        }  # TODO
        components = self.get_components(pipeline, workdir)
        build_info = {
            "job": sanatize_name(pipeline.attributes['ref']),
            "build": pipeline.attributes['id'],
            "name": "mesa-%s" % pipeline.attributes['id'],
            "start_time": get_timestamp(pipeline.attributes['created_at']),
            "end_time": get_timestamp(pipeline.attributes['updated_at']),
            "url": pipeline.attributes['web_url'],
            "result_path": ".",
            "revisions": revisions,
            "components": [c for (_, c) in components.items()]
            }
        return build_info

    def create_tarball(self, pipeline):
        def clean(workdir):
            for file in [
                workdir + "/build_info.json",
                workdir + "/artifacts.zip",
            ]:
                if os.path.exists(file):
                    try:
                        os.remove(file)
                    except OSError as error:
                        print("Error removing temporary files: %s" % error)

            if os.path.exists(workdir + os.sep + "results"):
                for root, adir, files in os.walk(os.path.normpath(
                                                     workdir + "/results"),
                                                 topdown=False):
                    for file in files:
                        try:
                            os.remove(root + os.sep + file)
                        except OSError as error:
                            print("Error removing temporary files: %s" % error)

        result_path = "/results"

        tmpdir = tempfile.mkdtemp()
        build_info = self.generate_build_info(pipeline, tmpdir)
        build_info_file = tmpdir + "/build_info.json"
        with open(build_info_file, "w") as fh:
            fh.write(json.dumps(build_info))

        comp_filename = "results_%s.%s.%s.tar" % (build_info["name"],
                                                  build_info["job"],
                                                  build_info["build"])
        comp_filename = sanatize_name(comp_filename)
        comp_filepath = "%s/%s" % (tmpdir, comp_filename)

        with tarfile.open(comp_filepath, "w") as tf:
            os.chdir(tmpdir)
            tf.add("build_info.json")
            for component in build_info["components"]:
                for artifact in component["artifacts"]:
                    os.chdir(tmpdir + result_path)
                    tf.add(artifact["file"])

        cmd = ["xz", "-T0", comp_filepath]

        ret = subprocess.call(cmd)
        comp_filepath += ".xz"
        if ret:
            raise RuntimeError("Compression with xz failed!")

        print("Results successfully compressed to: %s" % comp_filepath)

        clean(tmpdir)

        return comp_filepath
