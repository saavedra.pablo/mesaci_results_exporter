from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))


def read_file(path_segments):
    """Read a file from the package. Takes a list of strings to join to
    make the path"""
    file_path = os.path.join(here, *path_segments)
    with open(file_path) as f:
        return f.read()


def exec_file(path_segments):
    """Execute a single python file to get
    the variables defined in it"""
    result = {}
    code = read_file(path_segments)
    exec(code, result)
    return result


version = exec_file(("mesaci_results", "__init__.py"))["__version__"]

long_description = ""
try:
    long_description = open('README.md').read()
except Exception:
    pass

license = ""
try:
    license = open('LICENSE').read()
except Exception:
    pass


setup(
    name='mesaci-results-exporter',
    version=version,
    description='A exporter from the Freedesktop Mesa Gitlab to MesaCI',
    author='Pablo Saavedra',
    author_email='psaavedra@igalia.com',
    url='https://gitlab.freedesktop.org/saavedra.pablo/mesaci_results_exporter/',  # noqa: E501
    packages=find_packages(),
    package_data={
    },
    scripts=[
        "tools/export-mesa-gitlab-ci-results",
        "tools/webhook-mesa-gitlab-ci-result",
    ],
    zip_safe=False,
    install_requires=[
        "bottle",
        "python-gitlab",
    ],

    download_url='https://gitlab.freedesktop.org/saavedra.pablo/mesaci_results_exporter/-/archive/master/mesaci_results_exporter-master.tar.gz',  # noqa: E501
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
    ],
    long_description=long_description,
    license=license,
    keywords="python mesa ci cts exporter",
)
