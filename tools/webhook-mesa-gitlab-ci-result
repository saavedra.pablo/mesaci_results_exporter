#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Pablo Saavedra <psaavedra@igalia.com>
# Maintainer: Pablo Saavedra <psaavedra@igalia.com>

import gitlab
import logging
import os

from bottle import HTTPResponse, request, route, run, get, post
# from bottledaemon import daemon_run

from mesaci_results.utils import *

server = os.getenv("WEBHOOK_MESA_GITLAB_CI_SERVER", "0.0.0.0:8081")
WEBHOOK_MESA_GITLAB_CI_HOST, WEBHOOK_MESA_GITLAB_CI_PORT = server.split(":")

GITLAB_URL = os.getenv("GITLAB_URL", "http://gitlab.freedesktop.org")
GITLAB_TOKEN = os.getenv("GITLAB_TOKEN", "none")
GITLAB_WEBHOOK_SECRET_TOKEN = os.getenv("GITLAB_WEBHOOK_SECRET_TOKEN", "")
GITLAB_PROJECT = os.getenv("GITLAB_PROJECT", "mesa/mesa")

LOG_FILE = os.getenv("LOG_FILE", "webhook-mesa-gitlab-ci-result.log")
BOTTLE_LOG_FILE = os.getenv("BOTTLE_LOG_FILE", "bottle.log")
BOTTLE_PID = os.getenv("BOTTLE_PID", "bottle.pid")


def create_response(status, message):
    if status >= 400:
        logging.error(message)
    else:
        logging.debug(message)
    response = {
        "status": status,
        "message": message,
    }
    return HTTPResponse(status=status, body=json.dumps(response))


@post("/hooks/gitlab")
@get("/hooks/gitlab")
def hooks_gitlab():
    logging.info("======================")

    if request.headers.get("Content-Type") != "application/json":
        return create_response(400, "Invalid Content-Type: %s"
                                    % request.headers.get("Content-Type"))

    if GITLAB_WEBHOOK_SECRET_TOKEN:
        secret_token = request.headers.get("X-Gitlab-Token")
        if secret_token:
            if secret_token != GITLAB_WEBHOOK_SECRET_TOKEN:
                return create_response(403, "Secret token provided is invalid")
        else:
            return create_response(403, "Secret token is required")

    if request.headers.get("X-Gitlab-Event") != "Pipeline Hook":
        return create_response(200,
                               "This hook does nothing with this kind of " +
                               "events. Only Pipeline events are processed")

    pipeline_event = request.json

    project_id = pipeline_event.get("project", {}).get("id")
    if project_id != syncer.project.get_id():
        return create_response(200,
                               "Discarding event received from project: %s"
                               % project_id)

    object_kind = pipeline_event.get("object_kind")
    if object_kind != "pipeline":
        return create_response(200,
                               "Discarding event received with object: %s"
                               % object_kind)

    try:
        pipeline_id = pipeline_event["object_attributes"]["id"]
        pipeline = syncer.project.pipelines.get(pipeline_id)
        tarball = syncer.create_tarball(pipeline)
    except Exception as error:
        return create_response(500,
                               "Problems processing the pipeline #%s"
                               % pipeline_id)

    return create_response(200,
                           "Results tarball for pipeline #%s generated: %s"
                           % (pipeline_id, tarball))


if __name__ == "__main__":
    last_pipeline = 1
    limit = 50

    logging.basicConfig(filename=LOG_FILE,
                        format="%(asctime)s %(message)s",
                        datefmt="%m/%d/%Y %I:%M:%S %p",
                        level=logging.DEBUG)

    gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
    project = gl.projects.get(GITLAB_PROJECT)
    syncer = GitlabSyncer(project)

    # daemon_run(host="localhost",
    #            port=8989,
    #            logfile=BOTTLE_LOG_FILE,
    #            pidfile=BOTTLE_PID)
    run(host=WEBHOOK_MESA_GITLAB_CI_HOST, port=WEBHOOK_MESA_GITLAB_CI_PORT)
